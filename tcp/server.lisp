(import urntils/bindings/luasocket luasocket)


(defun bind! (address port handler err-handler)
  (with (socket (luasocket/bind! (or address "*") (or port 80)))
    (while true
      (let* [(client (self socket :accept))
             (request (list (self client :receive)))
             (request-str (car request))
             (request-err (cadr request))]
        (if request-err
          (when err-handler (err-handler request-err))
          (with (response-str (handler request-str client))
            (self client :send response-str)))
        (self client :close)))))
