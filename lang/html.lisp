(import string ())

(defun escape (x)
  (id (gsub x "[\">/<'&]"
            { "&"  "&amp;"
              ">"  "&gt;"
              "<"  "&lt;"
              "\"" "&quot;"
              "'"  "&#39;" })))

(defmacro render (elem)
  (case elem
    [((as key? ?nm) ?attr . ?body)
     (unless (eq? (car attr) 'struct-literal)
       (error! (.. "Expected a map as the second element, got " (pretty attr))))
     `(.. ,(.. "<" (sub (pretty nm) 2)
               (let* [(out "")]
                 (for i 2 (# attr) 2
                   (set! out (.. out " " (sub (get-idx (nth attr i) :contents) 2)
                                 "=" (get-idx (nth attr (+ i 1)) :contents))))
                 out)
               ">")
          (render ,body)
          ,(.. "</" (sub (pretty nm) 2) ">"))]
    [(unquote ?x)
     x]
    [?x
      (cond
        [(list? x)
         `(.. ,@(let* [(out '())]
                  (for i 1 (# x) 1
                    (push-cdr! out `(render ,(nth x i))))
                  out))]
        [(string? x)
         (escape (let* [(cont (get-idx x :contents))]
                   (sub cont 2 (- (#s cont) 1))))]
        [(symbol? x)
         (escape (get-idx x :contents))]
        [true (error! (.. "you wot: " (pretty x)))])]))
