(import web/tcp/server tcp)
(import table)
(import lua/os os)

(defun generate-response (data status-code headers)
  (let* [(status-code (or status-code "200 OK"))
         (headers (table/merge { "Content-Type"   "text/html; charset=utf-8"
                                 "Cache-Control"  "no-cache"
                                 "Server"         "unnamed"
                                 "Connection"     "Close"
                                 "Date"           (os/date "%a %b  %d %X %Y")
                                 "Content-Length" (n data) }
                               (or headers {})))
         (response (cons (.. "HTTP/1.1 " status-code)
                         (append (map (lambda (header-key)
                                        (.. header-key ": " (.> headers header-key)))
                                      (keys headers))
                                 (list "" data))))]
    (concat response "\r\n")))

(defun bind! (address port handler)
  (tcp/bind! address port
    (lambda (request-str)
      (let* [(request-parts (string/split request-str " "))
             (request-type (car request-parts))
             (request-path (cadr request-parts))
             (request-protocol (caddr request-parts))]
        (generate-response (handler request-path request-type request-protocol))))))
